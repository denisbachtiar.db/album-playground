import EasyAccess, { defaultMutations } from "vuex-easy-access";

export const state = () => ({
  datas_album: [],
  datas_user: [],
  datas_photo: [],

  loading: false,
  loading_user: false,
  loading_photo: false
});

export const mutations = {
  ...defaultMutations(state()),
  loading: (state, payload) => {
    state.loading = payload;
  },
  loading_user: (state, payload) => {
    state.loading_user = payload;
  },
  loading_photo: (state, payload) => {
    state.loading_photo = payload;
  },
  datas_album: (state, payload) => {
    state.datas_album = payload;
  },
  datas_user: (state, payload) => {
    state.datas_user = payload;
  },
  datas_photo: (state, payload) => {
    state.datas_photo = payload;
  }
};

export const plugins = [EasyAccess()];

export const actions = {
  loadAlbums({ dispatch }, params) {
    dispatch("set/loading", true);

    return this.$axios
      .get(`https://jsonplaceholder.typicode.com/albums`, {
        params: params
      })
      .then(res => {
        dispatch("set/datas_album", res.data);
        return true;
      })
      .catch(err => {
        return false;
      })
      .finally(() => {
        dispatch("set/loading", false);
      });
  },

  loadUsers({ dispatch }, id) {
    dispatch("set/loading_user", true);

    const param = id ? `?id=${id}` : "";
    return this.$axios
      .get(`https://jsonplaceholder.typicode.com/users${param}`)
      .then(async res => {
        await dispatch("set/datas_user", res.data);
        dispatch("set/loading_user", false);
        return true;
      })
      .catch(err => {
        dispatch("set/loading_user", false);
        return false;
      })
      .finally(() => {});
  },

  loadPhotos({ dispatch }, id) {
    dispatch("set/loading_photo", true);

    const param = id ? `?albumId=${id}` : "";
    return this.$axios
      .get(`https://jsonplaceholder.typicode.com/photos${param}`)
      .then(async res => {
        await dispatch("set/datas_photo", res.data);
        dispatch("set/loading_photo", false);
        return true;
      })
      .catch(err => {
        dispatch("set/loading_photo", false);
        return false;
      })
      .finally(() => {});
  },

  addToFavourite({ dispatch }, item) {
    let photos = !localStorage["favourites-photo"]
      ? []
      : JSON.parse(localStorage["favourites-photo"]);
    photos.push(item);
    localStorage.setItem("favourites-photo", JSON.stringify(photos));
  },

  removeFavourite({ dispatch }, item) {
    var photos = JSON.parse(localStorage["favourites-photo"]);
    var index = photos.findIndex(val => {
      return val.id === item.id && val.albumId === item.albumId;
    });
    if (index !== -1) photos.splice(index, 1);
    localStorage.setItem("favourites-photo", JSON.stringify(photos));
  },

  sendComment({ dispatch }, comment) {
    let comment_list = !localStorage["comment-list"]
      ? []
      : JSON.parse(localStorage["comment-list"]);
    comment_list.push(comment);
    localStorage.setItem("comment-list", JSON.stringify(comment_list));
  }
};
